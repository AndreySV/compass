'''
Simple GTK compass app for Mobile Linux

Copyright (C) 2021-2023 Leonardo G. Trombetta and Marco L. Trombetta

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import datetime
import json
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import pandas as pd
import subprocess
import time

class device:
    
    def __init__(self, testmode=0):
        self.MAGN_SCALE = 6842
        self.id = -1
        self.name = ""
        self.f_list = []
        self.mount_matrix = np.identity(3)
        self.mount_matrix_from_driver = False

        devices = []
        magn_chips = ["LIS3MDL", "AF8133J", "LSM9DS1_MAGN"]
        
        BASEPATH = "/sys/bus/iio/devices/iio:device"

        for i in range(4):
            if os.path.exists(os.path.join(BASEPATH+str(i), "name")):
                name = open(os.path.join(BASEPATH+str(i), "name")).read().strip().upper()

                if name in magn_chips:
                    if os.path.exists(os.path.join(BASEPATH+str(i), "sampling_frequency_available")):
                        f_list = list(open(os.path.join(BASEPATH+str(i), "sampling_frequency_available")).read().strip().split(" "))
                    else:
                        f_list = []

                    mount_matrix_from_driver = False

                    if name == 'LIS3MDL':
                        if os.path.exists(os.path.join(BASEPATH+str(i), "mount_matrix")):
                            mount_matrix = np.array(np.matrix(open(os.path.join(BASEPATH+str(i), "mount_matrix")).read()))            
                            mount_matrix_from_driver = True
                        else:
                            mount_matrix = np.array([[0,1,0], [-1,0,0], [0,0,1]]) # LIS3MDL on the OG PinePhone (up to v1.2)
                    
                    elif name == 'AF8133J':
                        if os.path.exists(os.path.join(BASEPATH+str(i), "in_mount_matrix")):
                            mount_matrix = np.array(np.matrix(open(os.path.join(BASEPATH+str(i), "in_mount_matrix")).read()))
                            mount_matrix_from_driver = True
                        else:
                            if os.cpu_count() == 4:
                                mount_matrix = np.array([[0,-1,0], [-1,0,0], [0,0,-1]]) # AF8133J on the OG PinePhone Beta (v1.2b)
                            elif os.cpu_count() == 6:
                                mount_matrix = np.array([[0,1,0], [-1,0,0], [0,0,1]]) # AF8133J on the PinePhone Pro
                    
                    elif name == 'LSM9DS1_MAGN':
                        mount_matrix = np.array([[1,0,0], [0,-1,0], [0,0,-1]]) # LSM9DS1_MAGN on the Librem 5
                        
                    devices.append([i, name, f_list, mount_matrix, mount_matrix_from_driver])

        if len(devices)>0 and testmode==0:
            j = 0

            self.id = devices[j][0]
            self.path = BASEPATH+str(self.id)
            self.name = devices[j][1]
            self.f_list = devices[j][2]
            self.mount_matrix = devices[j][3]
            self.mount_matrix_from_driver = devices[j][4]

    def getMagnValues(self):
        # First get magnetic field in chip coordinates
        if self.id in range(4):
            chipBx = int(open(os.path.join(self.path, "in_magn_x_raw")).read())
            chipBy = int(open(os.path.join(self.path, "in_magn_y_raw")).read())
            chipBz = int(open(os.path.join(self.path, "in_magn_z_raw")).read())
        else:
            chipBx = (np.random.random(1).item()*2-1)*self.MAGN_SCALE
            chipBy = (np.random.random(1).item()*2-1)*self.MAGN_SCALE
            chipBz = (np.random.random(1).item()*2-1)*self.MAGN_SCALE

        chipB = np.array([chipBx, chipBy, chipBz])

        # Apply mount_matrix to rotate to phone coordinates
        B = self.mount_matrix.dot(chipB)

        return B

    def setSamplingFreq(self, f=1):
        if self.id >= 0 and len(self.f_list) > 0:
            if (str(int(f)) in self.f_list):
                try:
                    open(os.path.join(self.path, "sampling_frequency"), 'w').write(str(int(f)))
                except Exception:
                    raise Exception("* Cannot change sampling frequency")
            else:
                raise Exception("* Invalid sampling frequency")

class compass:

    def calibrationRun(self, on_calibration_end, t=30):
        data = []
        interval = 0.1

        self.calib_countdown = t
        starttime = datetime.datetime.now()

        while self.calib_countdown > 0:
            data.append(self.B)
            elapsed = (datetime.datetime.now()-starttime).total_seconds()
            self.calib_countdown = t - elapsed
            if self.GUI: self.updater('Stop (%02d)' % int(self.calib_countdown), pos=5)
            time.sleep(interval)

        if len(data) > 0:
            df = pd.DataFrame(data)

            meanBx = int(df[0].mean()+self.B0[0])
            meanBy = int(df[1].mean()+self.B0[1])
            meanBz = int(df[2].mean()+self.B0[2])
        else:
            meanBx = 0
            meanBy = 0
            meanBz = 0

        self.calib_countdown = 0
        on_calibration_end([meanBx, meanBy, meanBz], elapsed=elapsed)

    def setOffset(self, offset=[0, 0, 0]):
        self.B0 = np.array(offset) # Hardiron offset

    def drawCompassCanvas(self):
        theta = np.linspace(0, 2 * np.pi, 100)
        t = np.linspace(0, 1, 10)

        if self.GUI:
            self.ax.add_patch(plt.Circle((0, 0), 1, fc='black', zorder=-1))
            self.ax.plot(np.cos(theta), np.sin(theta), color='gray', linewidth=5)

            self.ax.plot(2*t-1, t*0, color='white', zorder=0)
            self.ax.plot(t*0, 2*t-1, color='white', zorder=0)

            self.ax.plot(np.cos(theta)*(2/3), np.sin(theta)*(2/3), color='grey', linewidth=0.7, zorder=0)
            self.ax.plot( np.sqrt(3)*(2*t-1)/2, (2*t-1)/2, color='grey', linewidth=0.7, zorder=0)
            self.ax.plot( np.sqrt(3)*(2*t-1)/2, -(2*t-1)/2, color='grey', linewidth=0.7, zorder=0)
            self.ax.plot( (2*t-1)/2, np.sqrt(3)*(2*t-1)/2, color='grey', linewidth=0.7, zorder=0)
            self.ax.plot( (2*t-1)/2, -np.sqrt(3)*(2*t-1)/2, color='grey', linewidth=0.7, zorder=0)

    def removeNeedle(self):
        if self.GUI:
            self.needle[0].remove()
            self.needle[1].remove()
            self.needle[2].remove()

    def drawNeedle(self, Px, Py):
        points = np.array([[(1/10)*Py, -(1/10)*Px], [-(1/10)*Py, (1/10)*Px], [(5/6)*Px, (5/6)*Py]])
        
        if self.GUI:
            self.needle = [plt.Polygon(points, color='r', zorder=1),plt.Polygon(-points, color='w', zorder=1),plt.Circle((0, 0), 0.03, fc='black', zorder=2)]
            self.ax.add_patch(self.needle[0])
            self.ax.add_patch(self.needle[1])
            self.ax.add_patch(self.needle[2])

    def setSamplingFreq(self, f):
        try:
            self.magn.setSamplingFreq(int(f))
            print("* Sampling frequency set to %d Hz" % int(f))
            if self.GUI: self.updater("Sampling frequency set to %d Hz" % int(f), color='green', pos=3)
        except Exception as e:
            print(e)
            if self.GUI: self.updater(e, color='red', pos=3)
    
    def on_close(self):
        print("\033[8E")

        self.setSamplingFreq(1)

        print('* Closing app')

    def __init__(self, updater, testmode=0, gui=True, offset=[0, 0, 0]):
        self.TEST=testmode
        self.GUI=gui
        self.updater=updater
        self.calib_countdown = 0
        
        if self.GUI:
            mpl.rcParams['toolbar'] = 'None'

            print("* Using", mpl.get_backend(), "matplotlib backend")

            self.fig = plt.figure('Compass', facecolor=(0,0,0,0))

            ax = self.fig.add_axes([0,0,1,1], facecolor="black", frameon=False)
            ax.set_xlim([-1.2,1.2])
            ax.set_ylim([-1.2,1.2])
            ax.set_aspect(1)
            
            self.ax=ax

            self.drawCompassCanvas()
            self.drawNeedle(0,1)
        else:
            self.fig = None
            
        self.magn = device(testmode)

        self.setOffset(offset)
        print("* Hardiron offset:\n  B0[raw] =", self.B0)

        if self.magn.id > 0:
            print("* %s device found with id: %d" % (self.magn.name, self.magn.id))
            print("* Mount matrix (%s):\n  %s" % ('driver' if self.magn.mount_matrix_from_driver else 'hardcoded', str(self.magn.mount_matrix)[1:-1].replace('\n', '\n ')))
            if self.GUI: self.updater('Magnetometer found: %s' % self.magn.name, color='green', pos=2)
            
            self.setSamplingFreq(80)
        else:
            print("* No device found. Switched to\n  simulation mode.")
            if self.GUI: self.updater('No magnetometer found: Running in simulation mode', color='red', pos=2)

    def updateMagValues(self):
        self.B = (self.magn.getMagnValues() - self.B0)
        self.absB = (self.B[0]**2 + self.B[1]**2 + self.B[2]**2)**(1/2)
        self.absBxy = (self.B[0]**2 + self.B[1]**2)**(1/2)        
        
        print("\nReadings:\n")
        print('Bx[raw] = '+str(int(self.B[0])).ljust(10)+'Bx[Gauss] = '+str(round(self.B[0]/self.magn.MAGN_SCALE, 3)))
        print('By[raw] = '+str(int(self.B[1])).ljust(10)+'By[Gauss] = '+str(round(self.B[1]/self.magn.MAGN_SCALE, 3)))
        print('Bz[raw] = '+str(int(self.B[2])).ljust(10)+'Bz[Gauss] = '+str(round(self.B[2]/self.magn.MAGN_SCALE, 3)))
        print('B [raw] = '+str(int(self.absB)).ljust(10)+'B [Gauss] = '+str(round(self.absB/self.magn.MAGN_SCALE, 3)))
        print("\033[8F")

        if self.GUI: self.updater("Bx[Gauss] = %s\nBy[Gauss] = %s\nBz[Gauss] = %s" % (str(round(self.B[0]/self.magn.MAGN_SCALE, 3)).rstrip("0"), str(round(self.B[1]/self.magn.MAGN_SCALE, 3)).rstrip("0"), str(round(self.B[2]/self.magn.MAGN_SCALE, 3)).rstrip("0")), pos=4)

    def updateBearing(self,i):
        self.updateMagValues()

        #Needle's North as a point P in the unitary circle.
        Px = self.B[0]/self.absBxy
        Py = self.B[1]/self.absBxy

        self.removeNeedle()
        self.drawNeedle(Px,Py)
