# main.py
#
# Simple GTK compass app for Mobile Linux
#
# Copyright 2021-2023 Leonardo G. Trombetta and Marco L. Trombetta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import gi
import os
import sys
import xdg.BaseDirectory

gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '1')

from .compass import compass
from gi.repository import Gtk, Gio, GLib, GdkPixbuf
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)
from matplotlib.figure import Figure
from matplotlib.colors import ColorConverter
from threading import Timer
from .window import CompassWindow


class RepeatTimer(Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class Application(Gtk.Application):
    def __init__(self, testmode, version):
        super().__init__(application_id='com.gitlab.lgtrombetta.Compass',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.version = version
        self.testmode = testmode

        configdir = xdg.BaseDirectory.xdg_config_home
        self.configfile = os.path.join(configdir, "compass.conf")
        self.config = configparser.ConfigParser()

    def delete_event(self, widget, event, donnees=None):
        self.cmp.on_close()
        self.timer.cancel()
        self.quit()

        return False

    def refresh(self):
        self.cmp.updateBearing(0)
        self.canvas.draw()

    def setDarkTheme(self, dark=True):
        win = self.props.active_window

        if isinstance(win, CompassWindow):
            win.settings.set_property("gtk-application-prefer-dark-theme", dark)
            style = win.get_style_context()
            bg_colour = style.get_background_color(
            Gtk.StateType.NORMAL).to_color().to_floats()
            cc = ColorConverter()
            cc.to_rgba(bg_colour)
            self.cmp.fig.patch.set_facecolor(bg_colour)

        self.save_config(section="settings",key="THEME",value=("dark" if dark else "light"), reload=True)

    def update_label(self, text, color='grey', pos=1):
        win = self.props.active_window

        if isinstance(win, CompassWindow):
            if pos == 1:
                win.toplabel1.set_markup("<span foreground='%s'>%s</span>" % (color, text))
            elif pos == 2:
                win.toplabel2.set_markup("<span foreground='%s'>%s</span>" % (color, text))
            elif pos == 3:
                win.toplabel3.set_markup("<span foreground='%s'>%s</span>" % (color, text))
            elif pos == 4:
                #win.bottomlabel.set_markup("<span foreground='%s'>%s</span>" % (color, text))
                win.bottomlabel.set_label("%s" % text)
            elif pos == 5:
                GLib.idle_add(win.calibratebtn.set_label, text)

    def load_config(self, output=False):
        if not os.path.exists(self.configfile):
            self.config["settings"] = {"VERSION": self.version, "THEME": "dark"}
            self.config["device"] = {"NAME": "", "MAGN_X_OFFSET": 0, "MAGN_Y_OFFSET": 0, "MAGN_Z_OFFSET": 0}

            with open(self.configfile, "w") as f:
                self.config.write(f)

        self.config.read(self.configfile)

        self.THEME=self.config["settings"]["THEME"]

        self.MAGN_X_OFFSET=int(self.config["device"]["MAGN_X_OFFSET"])
        self.MAGN_Y_OFFSET=int(self.config["device"]["MAGN_Y_OFFSET"])
        self.MAGN_Z_OFFSET=int(self.config["device"]["MAGN_Z_OFFSET"])

        if output:
            if (self.MAGN_X_OFFSET == 0 and self.MAGN_X_OFFSET == 0 and self.MAGN_X_OFFSET == 0):
                print("* No calibration data found.\n Run calibration tool.")
                self.update_label('No calibration data found. Run Settings > Calibrate', color='red', pos=1)
            else:
                print("* Calibration data loaded")
                self.update_label('Calibration data loaded', color='green', pos=1)

    def save_config(self, section, key, value, reload=False):
        self.config[section][key] = value
        with open(self.configfile, "w") as f:
            self.config.write(f)

        if reload:
            self.load_config()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = CompassWindow(application=self)

        self.load_config(output=True)

        self.cmp=compass(updater=self.update_label, testmode=self.testmode, offset=[self.MAGN_X_OFFSET, self.MAGN_Y_OFFSET, self.MAGN_Z_OFFSET])

        self.save_config(section="device",key="NAME",value=self.cmp.magn.name, reload=True)

        print("* GUI theme:", self.THEME)

        # The Switch button is initially "on"
        if self.THEME == "dark":
            # Since the Switch button is in the correct position, we just call setDarkTheme(True)
            self.setDarkTheme(True)
        else:
            # The Switch button needs to be changed to "off". First we set THEME="dark" and then trigger the button flip event. From there, the setDarkTheme(False) is called and we end up with THEME="light" as
            self.THEME = "dark"
            win.dark_mode_switch.set_state(False)

        self.canvas = FigureCanvas(self.cmp.fig)  # a Gtk.DrawingArea
        self.canvas.set_halign(Gtk.Align.CENTER)
        self.canvas.set_valign(Gtk.Align.CENTER)
        self.canvas.set_size_request(360, 360)

        win.canvas.add(self.canvas)

        win.connect("delete_event", self.delete_event)

        self.timer = RepeatTimer(0.3, self.refresh)
        self.timer.start()

        win.show_all()


def main(version):
    print('Compass v%s\nhttps://gitlab.com/lgtrombetta/compass\nReleased under the GPLv3 license\n' % version)

    TEST = 0	# Testing mode. 0: Read the sensor. 1: Generate random readings.

    if len(sys.argv) > 0:
        for arg in sys.argv:
            if str(arg) == "-test":
            	TEST = 1

    app = Application(testmode=TEST, version=version)

    return app.run(sys.argv)
